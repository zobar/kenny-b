#!/usr/bin/env python

from kenny import Kenny
from kenny.couchbase import Bucket
from kenny.py2neo import Graph
from py2neo import watch
import numpy
import sys

def add(kenny, files):
    for file in files:
        for fragment in kenny.load(file):
            print "%s: %s" % (file, id)

if __name__ == '__main__':
    # watch('py2neo.cypher')

    kenny = Kenny(graph=Graph(bucket=Bucket('kenny'),
                              uri='http://neo4j:eTc!Ill*g2!4@localhost:7474/db/data/'))

    list(kenny.split(kenny.load(file) for file in sys.argv[1:]))
