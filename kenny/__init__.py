from __future__ import absolute_import

import av
import functools
import itertools
from kenny import file, fingerprint
import numpy

class Kenny:
    def __init__(self, graph, dtype='h', format='s16', layout=2, level=10,
                 rate=44100, size=8):
        self.dtype = dtype
        self.fingerprint = functools.partial(fingerprint.fragment, level)
        self.format = format
        self.graph = graph
        self.layout = layout
        self.level = level
        self.rate = rate
        self.size = size

    def load(self, file_name):
        samples, fingerprints = itertools.tee(file.fragments(file.open(file_name),
                                                             dtype=self.dtype,
                                                             format=self.format,
                                                             layout=self.layout,
                                                             rate=self.rate,
                                                             size=2 ** self.level))
        fingerprints = itertools.imap(self.fingerprint, fingerprints)

        return self.sequence(self.graph.load(samples, fingerprints))

    def sequence(self, fragments):
        fragments = list(fragments)
        fragment_ids = [fragment[0] for fragment in fragments]
        sequence_fingerprint = fingerprint.combine([fragment[1] for fragment in fragments])
        return self.graph.sequence(fragment_ids, sequence_fingerprint)

    def split(self, sequence_ids):
        for fragments in self.graph.split(self.size, sequence_ids):
            yield self.sequence(zip(*fragments))
