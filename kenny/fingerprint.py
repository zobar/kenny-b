import numpy
import pywt

def channel(level, channel):
    wavelet = pywt.wavedec(channel, 'haar', level=level)
    result = [int(numpy.round(numpy.sum(numpy.abs(coefficients))))
              for coefficients in wavelet]
    return result

def combine(fingerprints):
    return numpy.sum(fingerprints, axis=0)

def fragment(level, fragment):
    return numpy.ravel([channel(level, c) for c in numpy.transpose(fragment)],
                       order='F')
