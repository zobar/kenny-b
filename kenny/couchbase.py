from __future__ import absolute_import

import couchbase
from couchbase import Couchbase, exceptions
from kenny.util import batches, hash

class Bucket:
    def __init__(self, *args, **kwargs):
        self.bucket = Couchbase.connect(*args, **kwargs)

    def add_samples(self, samples):
        items = {}
        for sample in samples:
            raw = bytes(buffer(sample))
            key = hash(raw)
            items[key] = raw
            yield key
        try:
            self.bucket.add_multi(items, format=couchbase.FMT_BYTES)
        except exceptions.KeyExistsError:
            pass
