from __future__ import absolute_import

import av
import numpy

def frames(container):
    return (frame for packet in container.demux()
                  for frame in packet.decode()
                  if isinstance(frame, av.AudioFrame))

def fragments(container, dtype, format, layout, rate, size):
    dtype = numpy.dtype(dtype)
    format = av.AudioFormat(format).packed
    layout = av.AudioLayout(layout)
    return rechunked(size,
                     ndarrays(dtype,
                              resampled(format, layout, rate,
                                        frames(container))))

def metadata(file):
    return file.metadata

def ndarray(dtype, frame):
    channels = len(frame.layout.channels)
    plane, = frame.planes
    samples = frame.samples
    return numpy.frombuffer(plane, dtype).reshape(samples, channels)

def ndarrays(dtype, packed):
    return (ndarray(dtype, frame) for frame in packed)

def open(file):
    return av.open(file)

def rechunked(size, chunked):
    buffer = None
    buffer_start = 0
    for chunk in chunked:
        if buffer is None:
            buffer = numpy.empty((size,) + chunk.shape[1:], chunk.dtype)
        chunk_start = 0
        chunk_size = len(chunk)

        while chunk_start < chunk_size:
            copy_length = min(size - buffer_start, chunk_size - chunk_start)
            buffer_end = buffer_start + copy_length
            chunk_end = chunk_start + copy_length

            buffer[buffer_start:buffer_end] = chunk[chunk_start:chunk_end]

            if buffer_end == size: yield buffer

            buffer_start = buffer_end % size
            chunk_start = chunk_end

def resampled(format, layout, rate, frames):
    resample = None
    for frame in frames:
        if resample is None:
            resample = av.AudioResampler(format, layout, rate).resample
        yield resample(frame)
