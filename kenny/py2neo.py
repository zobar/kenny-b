from __future__ import absolute_import

import hashlib
import itertools
from kenny.util import batches, hash
import py2neo

class Graph:
    def __init__(self, bucket, *args, **kwargs):
        self.add_samples = bucket.add_samples
        cypher = py2neo.Graph(*args, **kwargs).cypher
        self.begin = cypher.begin
        self.execute = cypher.execute
        with self.begin() as transaction:
            transaction.append('''
                CREATE CONSTRAINT ON (fragment:Fragment)
                ASSERT fragment.id IS UNIQUE''')
            transaction.append('''
                CREATE CONSTRAINT ON (song:Song)
                ASSERT song.id IS UNIQUE''')
            transaction.append('''
                CREATE CONSTRAINT ON (tone:Tone)
                ASSERT tone.id IS UNIQUE''')

    def load(self, samples, fingerprints):
        with self.begin() as transaction:
            fragment_fingerprints = {}
            for i, (sample_id, fingerprint) in enumerate(itertools.izip_longest(self.add_samples(samples), fingerprints)):
                fragment_fingerprints[sample_id] = tuple(fingerprint)
                yield (i, sample_id), fingerprint

            transaction.append('''
                FOREACH(info IN {fingerprints} |
                        MERGE (fragment:Fragment {id: info[0]})
                        SET fragment.fingerprint = info[1])''',
                fingerprints=fragment_fingerprints.items())

    def sequence(self, fragment_ids, fingerprint):
        with self.begin() as transaction:
            sha = hashlib.sha256()
            for i, fragment_id in fragment_ids:
                sha.update(fragment_id)
            id = sha.hexdigest()
            tone_id = hash(bytes(buffer(fingerprint)))

            transaction.append('''
                MERGE (sequence:Sequence {id: {id}})
                MERGE (tone:Tone {id: {tone_id}})
                SET tone.fingerprint = {fingerprint}
                MERGE (sequence)-[:STARTS_LIKE]->(tone)
                MERGE (sequence)-[:ENDS_LIKE]->(tone)
                FOREACH(info IN {fragment_info} |
                        MERGE (fragment:Fragment {id: info[1]})
                        MERGE (sequence)-[:CONTAINS {i: info[0]}]->(fragment))''',
                id=id,
                fingerprint=tuple(fingerprint),
                fragment_info=fragment_ids,
                tone_id=tone_id)

            return id

    def split(self, size, sequence_ids):
        with self.begin() as transaction:
            transaction.append('''
                MATCH (sequence:Sequence)-[contains:CONTAINS]->(fragment:Fragment)
                WHERE sequence.id IN {ids}
                WITH sequence,
                     contains.i / {size} AS grouping,
                     COLLECT([contains.i % {size}, fragment.id]) AS fragments,
                     COLLECT(fragment.fingerprint) AS fingerprints
                RETURN fragments, fingerprints''',
                ids=list(sequence_ids),
                size=size)
            return transaction.process()[-1]

def add_song(transaction, metadata, sequence_id):
    transaction.append('''
        MERGE (song:Song {id: {id}})
        MERGE (sequence:Sequence {id: {id}})
        SET song += {properties}
        MERGE (song)-[:HAS]->(sequence)''',
        id=sequence_id,
        properties=metadata)

    return sequence_id
