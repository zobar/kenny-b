#!/usr/bin/env python

import aifc
import av
import itertools
import numpy
import pywt
import random
import sys
import wave

def as_ndarray(dtype, frames):
    return numpy.array([
        tuple(numpy.frombuffer(plane, dtype) for plane in frame.planes)
        for frame in frames])

def audio_file(file):
    return (frame for packet in av.open(file).demux()
                  for frame in packet.decode()
                  if isinstance(frame, av.AudioFrame))

def chunk(samples, input):
    fifo = av.AudioFifo()
    input = iter(input)
    fifo.write(next(input))

    def next_frame():
        while fifo.samples < samples:
            fifo.write(next(input))
        return fifo.read(samples)

    output_frame = next_frame()
    while output_frame:
        yield output_frame
        output_frame = next_frame()

def doubled(ndarray):
  chunks, channels, half_size = ndarray.shape
  if chunks % 2:
    ndarray = ndarray[:-1]
  return numpy.reshape(ndarray, (chunks / 2, channels, half_size * 2))

def overlapped(ndarray):
    chunks, channels, half_size = ndarray.shape
    return numpy.reshape(numpy.repeat(ndarray, 2, axis=0)[1:-1],
                         (chunks - 1, channels, half_size * 2))

def resample(format, layout, rate, input):
    resample = av.AudioResampler(format, layout, rate).resample
    return (resample(frame) for frame in input)

class Library:
    def __init__(self):
        self.granules = []

    def __iter__(self):
        return iter(self.granules)

    def add(self, granule):
        if self.granules:
            def difference(index):
                return (granule - self.granules[index - 1]) + (granule - self.granules[index])
            index = min(xrange(len(self.granules)), key=difference)
            self.granules.insert(index, granule)
        else:
            self.granules.append(granule)

class Granule:
    def __init__(self, short_samples, double_samples):
        self.samples = short_samples
        wavelet = pywt.wavedec(double_samples, 'haar')
        self.fingerprint = tuple(numpy.sum(numpy.abs(coefficients)) for coefficients in wavelet)

        self.leading_fingerprint, self.trailing_fingerprint = (
            tuple(half_fingerprint)
            for half_fingerprint in
                numpy.transpose([numpy.sum(numpy.abs(numpy.split(coefficients, 2)), axis=1)
                                 for coefficients in wavelet[2:]]))

    def __sub__(self, other):
        return numpy.sum(numpy.abs(numpy.subtract(self.fingerprint, other.fingerprint)))

    def after_difference(self, other):
        return numpy.sum(numpy.abs(numpy.subtract(self.leading_fingerprint, other.trailing_fingerprint)))

    def before_difference(self, other):
        return numpy.sum(numpy.abs(numpy.subtract(self.trailing_fingerprint, other.leading_fingerprint)))

def run():
    bands = 14
    chunk_size = 2 ** bands
    granules = []
    libraries = []
    main = Library()
    smoothing = []

    fade_in = numpy.sin(numpy.linspace(0, numpy.pi / 2.0, chunk_size / 2))
    fade_out = numpy.cos(numpy.linspace(0, numpy.pi / 2.0, chunk_size / 2))

    output = wave.open('tmp/drone-2.wav', 'w')
    output.setframerate(44100)
    output.setnchannels(1)
    output.setsampwidth(2)

    songs = [
        'wilson-phillips-hold-on',
        'it-must-have-been-love',
        'nothing-compares-2-u',
        'poison',
        'vogue',
        'vision-of-love',
        'another-day-in-paradise',
        'en-vogue-hold-on',
        'cradle-of-love',
        'blaze-of-glory'
    ]

    for song in songs:
        print 'loading %s' % song
        library = Library()
        frames = list(chunk(chunk_size / 2, audio_file('tmp/%s.mp3' % song)))
        short_frames = as_ndarray(numpy.dtype('h'),
                                  resample('s16p', 1, 44100, frames))
        double_frames = as_ndarray(numpy.dtype('d'),
                                   resample('dblp', 1, 44100, frames))

        for short_frame, double_frame in zip(doubled(short_frames), doubled(double_frames)):
            for short_plane, double_plane in zip(short_frame, double_frame):
                library.add(Granule(short_plane, double_plane))

        for short_frame, double_frame in zip(doubled(short_frames[1:]), doubled(double_frames[1:])):
            for short_plane, double_plane in zip(short_frame, double_frame):
                smoothing.append(Granule(short_plane, double_plane))

        library.granules.reverse()
        libraries.append(library.granules)

    print 'multiplexing'
    for i in xrange(max(len(library) for library in libraries)):
        for j in xrange(len(libraries)):
            library = libraries[j]
            if i < len(library):
                main.add(library[i])

    print 'smoothing'
    for old, new in itertools.izip(main, itertools.islice(main, 1, None)):
        def difference(granule):
            return granule.after_difference(old) + granule.before_difference(new)
        smooth = min(smoothing, key=difference)
        granules.append(old)
        granules.append(smooth)

    print 'saving'
    for old, new in itertools.izip(granules, itertools.islice(granules, 1, None)):
        old_samples = numpy.split(old.samples, 2)[1]
        new_samples = numpy.split(new.samples, 2)[0]
        mixed = numpy.clip(numpy.add(numpy.multiply(old_samples, fade_out),
                                     numpy.multiply(new_samples, fade_in)),
                           -0x8000, 0x7fff).astype(numpy.int16)
        output.writeframes(bytearray(mixed))

    output.close()

if __name__ == '__main__':
    run()
