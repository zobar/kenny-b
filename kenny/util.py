import hashlib
import itertools

def batches(size, iterable):
    iterable = iter(iterable)
    while True:
        item = iterable.next()
        yield itertools.chain([item], itertools.islice(iterable, 0, size - 1))

def hash(raw):
    digest = hashlib.sha256()
    digest.update(raw)
    return digest.hexdigest()
